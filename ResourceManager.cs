using System;
using OpenCL.Net;

using System.Diagnostics;
using System.Runtime.InteropServices;

using System.Reflection;

using NUnit.Framework;

namespace WIOComputingToolkit
{
	

	
	/// <summary>
	/// We need some functionality that will allow for generic loading and unloading things from memory
	/// So the idea will be to define GPUHandles that can be changed if we unload it.
	/// </summary>

	
	//the definition is exacly : is switch is on then my dual representation if invalid (CPU buffer <-> GPU buffer)
	//this defines behaviour for operations that can be buffered somewhere dually.
	public interface OpenCLBufferable{
	 	 bool GetChangeSwitch();
		 void SetChangeSwitch(bool new_value);
		 object GetData();
		 void Update();  
		 void SetDual(OpenCLBufferable x);
		 OpenCLBufferable GetDual();
	}
	
	//it is a prototype
	public static class GPUResourceManager
	{
		private static  Cl.IMem _READ_FLOAT_BUF = null;
		//
		public static Cl.IMem READ_FLOAT_BUF{
		 	get{
				if(_READ_FLOAT_BUF == null){
					Cl.ErrorCode error;
					_READ_FLOAT_BUF = Cl.CreateBuffer(ToolkitGlobals.DefaultOpenCLNode._context,
			                                            Cl.MemFlags.ReadWrite, 
			                                             new IntPtr(1 * Marshal.SizeOf (typeof(float))), out error);
			
					Assert.IsTrue(error == Cl.ErrorCode.Success);		
				}
				return _READ_FLOAT_BUF;
			}
		}
		
		public static float GetFLOATBUFValue(){


			
			float[] data = new float[1];
			Cl.Event clevent;
			Cl.ErrorCode error = Cl.EnqueueReadBuffer(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, 
			                     GPUResourceManager.READ_FLOAT_BUF, Cl.Bool.True, IntPtr.Zero, 
			                     new IntPtr(1 * Marshal.SizeOf (typeof(float))),data,0,null,out clevent);
			
			Assert.IsTrue (error == Cl.ErrorCode.Success);
			Cl.Finish (ToolkitGlobals.DefaultOpenCLNode._cmdQueue);
			return data[0];

		}

		
		/// <summary>
		/// Ensures the consistency open CL bufferable.
		/// </summary>
		public static void EnsureGPUConsistencyOpenCLBufferable(OpenCLBufferable obj){
			if(obj.GetChangeSwitch() && ((OpenCLBufferable)obj.GetDual()).GetChangeSwitch()) throw new System.Exception("Inconsistent state");
			
			if(obj.GetChangeSwitch()) ((OpenCLBufferable)obj.GetDual()).Update();
			
			if(obj.GetChangeSwitch()) obj.SetChangeSwitch(false);
		}		
		/// <summary>
		/// Ensures that CPU has got correct snapshot of GPU
		/// </summary>
		public static void EnsureCPUConsistencyOpenCLBufferable(OpenCLBufferable obj){
			if(obj.GetChangeSwitch() && ((OpenCLBufferable)obj.GetDual()).GetChangeSwitch()) throw new System.Exception("Inconsistent state");
			
			if(((OpenCLBufferable)obj.GetDual()).GetChangeSwitch()) ((OpenCLBufferable)obj).Update();
			
			if(((OpenCLBufferable)obj.GetDual()).GetChangeSwitch()) ((OpenCLBufferable)obj.GetDual()).SetChangeSwitch(false);
		}			
		/// <summary>
		/// Ensures the consistency open CL bufferable.
		/// </summary>
		public static void EnsureConsistencyOpenCLBufferable(OpenCLBufferable obj){
			if(obj.GetChangeSwitch() && ((OpenCLBufferable)obj.GetDual()).GetChangeSwitch()) throw new System.Exception("Inconsistent state");
			
			if(obj.GetChangeSwitch()) ((OpenCLBufferable)obj.GetDual()).Update();
			else if(((OpenCLBufferable)obj.GetDual()).GetChangeSwitch()) ((OpenCLBufferable)obj).Update();
			
			
			
			
			if(obj.GetChangeSwitch()) obj.SetChangeSwitch(false);
			if(((OpenCLBufferable)obj.GetDual()).GetChangeSwitch()) ((OpenCLBufferable)obj.GetDual()).SetChangeSwitch(false);
		}
		/// <summary>
		/// Just check if only one is inconsistent state
		/// </summary>
		public static void EnsureConsistencyWeakOpenCLBufferable(OpenCLBufferable obj){
			if(obj.GetChangeSwitch() && ((OpenCLBufferable)obj.GetDual()).GetChangeSwitch()) throw new System.Exception("Inconsistent state");
		}
	}
	
	//NOTE : all reads and writes are blocking now to prevent bugs (for performance check it)
	//NOTE : All reads and writes are consistent
	
	/// <summary>
	/// GPU resource buffer handle, used by objects to perform operations,
	/// In practice it will allow for reinterpretations of the same memory and might become useful
	/// 
	/// 
	/// Each GPUResource has a timestamp - what is the last moment it got writen
	/// </summary>
	public class OpenCLResourceBufferHandle<T> : OpenCLBufferable, IDisposable where T: struct {	
		/// <summary>
		/// Writes to buffer given array (cannot be longer, but can be shorter)
		/// </summary>
		public void WriteToBuffer(T[] input, bool sync = false){
			if(!sync && _dual != null && _dual.GetChangeSwitch())
				throw new System.Exception("Invalid dual modifying its buffer");
			
			
			Cl.Event clevent;
			Cl.ErrorCode error = Cl.EnqueueWriteBuffer(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, this.BufferPtr, Cl.Bool.True,IntPtr.Zero,
			                      new IntPtr(Marshal.SizeOf(typeof(T))*input.Length), input,0,null,out clevent);
			//Cl.Finish (ToolkitGlobals.DefaultOpenCLNode._cmdQueue);
			
			Assert.IsTrue(error == Cl.ErrorCode.Success);
			Cl.Finish (ToolkitGlobals.DefaultOpenCLNode._cmdQueue);
			if(!sync && _dual != null) this.ChangeSwitch = true; //definitely something has changed
		}
		
		public void Update(){			
			if(!this._dual.GetChangeSwitch()) return;
			
			this.WriteToBuffer((T[])this._dual.GetData(), sync: true); //force updating s
			this._dual.SetChangeSwitch(false);
		}
		
		
		/// <summary>
		/// Reads the buffer.
		/// </summary>
		/// <returns>
		/// The buffer.
		/// </returns>
		/// <param name='N'>
		/// N - number of items to retrieve
		/// </param>
		public T[] ReadBuffer(int N){
			if(_dual != null && _dual.GetChangeSwitch()){
				throw new System.Exception("Invalid dual reading its buffer");
			}
			
			T[] data = new T[N];
			Cl.Event clevent;
			Cl.ErrorCode error = Cl.EnqueueReadBuffer(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, 
			                     this.BufferPtr, Cl.Bool.True, IntPtr.Zero, 
			                     new IntPtr(N * Marshal.SizeOf (typeof(T))),data,0,null,out clevent);
			
			Assert.IsTrue (error == Cl.ErrorCode.Success);
			Cl.Finish (ToolkitGlobals.DefaultOpenCLNode._cmdQueue);
			return data;
		}
		
		public OpenCLResourceBufferHandle(int N, Cl.MemFlags memflag = Cl.MemFlags.ReadWrite){
			StackFrame callback = new StackFrame(2);
			ToolkitGlobals.Logger.Log (" ** Created OpenCL GPU buffer from " + callback.GetMethod().Name + " cheers **");
			
			
			this.N = N;			
			Cl.ErrorCode error;
			
			this.BufferPtr = Cl.CreateBuffer(ToolkitGlobals.DefaultOpenCLNode._context,
			                                            memflag, 
			                                             new IntPtr(N * Marshal.SizeOf (typeof(T))), out error);
			
			Debug.Assert(error == Cl.ErrorCode.Success);
			
			this.Size =  new IntPtr(N * Marshal.SizeOf (typeof(T)));
			this.ChangeSwitch = false;
		}
		
		

		
		//=========================================//
		public void Dispose(){
			Dispose (true);
			GC.SuppressFinalize(this);
		}
		bool _disposed = false;
		public void Dispose(bool disposing){
			if(!this._disposed){
				this._disposed = true;
				Cl.ReleaseMemObject(this.BufferPtr);
			}
		}
		
		public readonly IntPtr Size;
		public int N;
		public Cl.IMem BufferPtr;	
		public bool ChangeSwitch; //toggled if change occurs
		
		private OpenCLBufferable _dual = null;
		
		public object GetData(){
			return (object)this.ReadBuffer(this.N);
		}
		public void SetDual(OpenCLBufferable dual){
			this._dual =(OpenCLBufferable) dual;
			if((object)this._dual.GetDual() != (object)this) this._dual.SetDual(this);
		}
		public OpenCLBufferable GetDual(){
			return this._dual;
		}
		public bool GetChangeSwitch(){
			return ChangeSwitch;
		}
		public void SetChangeSwitch(bool new_value){
			this.ChangeSwitch = new_value;
		}
	}


	

}

