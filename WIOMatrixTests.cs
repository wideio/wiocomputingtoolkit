using System;
using NUnit.Framework;
using WIOGlobals;

using Emgu.CV;
using Emgu.CV.Util;

using System.Collections;
using System.Collections.Generic;
using System.Linq;


using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

using System.Drawing;
using System.Runtime.InteropServices;
namespace WIOComputingToolkit
{
	
	public class WIOMatrixTests
	{
		public static void Setup(){
			
		}
		public static void TestVectReduce(){
		
			float[,] reduce_small = new float[1000,	1];
			float[] reduce_small_test = new float[1000];
			for(int i=0;i<reduce_small.Length;++i) reduce_small_test[i] = reduce_small[i,0] = i+1;
			
			WIOOpenCLMatrix small = new WIOOpenCLMatrix(reduce_small);

			small.MApplyLinear(1.0f);
			
			ToolkitGlobals.Logger.Log (small[10,1]);
			
			float res = small.ReduceSum();
			
			Assert.AreEqual(res, reduce_small_test.Aggregate( (agr, x) => agr+x));
			
			Random rnd = new Random();
			
			float[,] reduce_big = new float[1000000,	1];
			float[] reduce_big_test = new float[1000000];
			for(int i=0;i<reduce_big.Length;++i) reduce_big[i,0] = reduce_big_test[i] = rnd.Next () % 10 ;
			
			WIOOpenCLMatrix big = new WIOOpenCLMatrix(reduce_big);

	
		    res = big.ReduceSum();
			
			Assert.AreEqual(res, reduce_big_test.Aggregate( (agr, x) => agr+x));			
			
			
			
			
		}
		
		public static void TestAdditionAndMApply(){
		
			WIOMatrix ones = new WIOOpenCLMatrix(new float[,]{{0,0},{0,0}});
			ones.MApplyExp(); //make more generic (mapply can be easily generic, with {0} and string format..)
			
			WIOMatrix add_one = new WIOOpenCLMatrix(new float[,]{{0,0},{2,2}});
			
			WIOMatrix result = ones + add_one;
		
			ToolkitGlobals.Logger.Log("result[2,2]="+result[2,2].ToString());
			Assert.IsTrue(result[2,2] == 3);

			result.Add(add_one);

			Assert.IsTrue(result[2,2] == 5);	
		}
		
		/// <summary>
		/// Tests matrix implementation using image (visual test)
		/// </summary>
		public static void TestOpenCLConvAndLinearImage(){

//			
//			float[,] dev_data = new float[100,100];
//			WIOMatrixTests._FillRnd(dev_data,range: 256);
//			WIOOpenCLMatrix rnd_img = new WIOOpenCLMatrix(dev_data);
//			
//			Utils.DisplayOWIOOpenCLMatrixAsImage(rnd_img);
			
			WIOOpenCLMatrix test_png = Utils.WIOOpenCLMatrixFromBitmap(new Bitmap(Image.FromFile("test.png")));
			Utils.DisplayOWIOOpenCLMatrixAsImage(test_png);
			WIOOpenCLMatrix mask = new WIOOpenCLMatrix(new float[,]{ {0,1,0},{1,4,1},{0,1,0}});;//(new float[,]{ {0,-1,0},{-1,5,-1},{0,-1,0}});
			test_png = (WIOOpenCLMatrix)test_png.Convolve(mask);
			test_png.MApplyLinear(1.0f,-20.0f);
			Utils.DisplayOWIOOpenCLMatrixAsImage(test_png);
					
		}
		public static void TestOpenCLMatrixMultTran(){
			//OpenCL Buffers are created automatically here
			WIOMatrix wm = new WIOOpenCLMatrix(new float[,]{{1,4},{2,5},{3,6}});//{{1,2,3},{4,5,6}} );
			wm.Transpose();
			WIOMatrix wm2 = new WIOOpenCLMatrix(new float[,]{{7,9,11},{8,10,12}});//{{7,8},{9,10},{11,12}});
			wm2.Transpose();
			WIOMatrix result = wm*wm2;
			Assert.IsTrue(wm[2,3]==6);
			Assert.IsTrue(result[1,1] == 58);
			Assert.IsTrue (result[2,2] ==154);
			

		}	
		
		
		public static void TestOpenCLMatrixAdditionSubTran(){
			//OpenCL Buffers are created automatically here
			WIOMatrix wm = new WIOOpenCLMatrix(new float[,]{{1,4},{2,5},{3,6}});//{{1,2,3},{4,5,6}} );
		
			WIOMatrix wm2 = new WIOOpenCLMatrix(new float[,]{{2,2,2},{1,1,1}});//{{7,8},{9,10},{11,12}});
			wm2.Transpose();
			WIOMatrix result = wm+wm2;
			Assert.IsTrue(wm[3,2]==6);
			Assert.IsTrue(result[1,1] == 3);
			wm.Substract(wm2,_self: true);
			
			Assert.IsTrue(wm[3,2]==5);
		}			
		
		public static void TestOpenCLMatrixMult(){
			//OpenCL Buffers are created automatically here
			WIOMatrix wm = new WIOOpenCLMatrix(new float[,]{{1,2,3},{4,5,6}} );
			WIOMatrix wm2 = new WIOOpenCLMatrix(new float[,]{{7,8},{9,10},{11,12}});
			WIOMatrix result = wm*wm2;
			Assert.IsTrue(wm[2,3]==6);
			Assert.IsTrue(result[1,1] == 58);
			Assert.IsTrue (result[2,2] ==154);
		}
		
		public static void TestOpenCLMatrixConvol(){
			WIOMatrix wm = new WIOOpenCLMatrix(new float[,]{{1,5,9,13},{2,6,10,14},{3,7,11,15},{4,8,12,16}});
			WIOMatrix mask = new WIOOpenCLMatrix(new float[,]{{0,1,2},{0.1f,0,0},{0,0,0}});
			WIOMatrix result = wm.Convolve(mask);
		
		
			
			ToolkitGlobals.Logger.Log("Convolution[2,2] = "+result[2,2].ToString());
			NUnit.Framework.Assert.IsTrue(result[2,2] == 38.7f);
		}
		
		private static void _FillRnd(float[,] A, int range = 3){
			Random rand = new Random();
            for (int i = 0; i < A.GetLength (0); i++)
			for (int j=0; j< A.GetLength(1);++j)
                A[i,j] = rand.Next() % range;	
		} 
		
		private static float[,] _MatrixMultNaive(float[,] M1, float[,] M2){
			int p = M1.GetLength(0);
			int q = M1.GetLength(1);
			int r = M2.GetLength(1);
			if (q != M2.GetLength(0)) throw new Exception("Matrix dimensions do not match for multiplication");
			
			float[,] resp = new float[p, r];
			for (int i = 0; i < p; i++)
			  for (int j = 0; j < r; j++)
			    for (int k = 0; k < q; k++)
			        resp[i, j] += M1[i, k] * M2[k, j];
			
			return resp;	
		}
		
		
		/// <summary>
		/// Tests the open CL matrix big with several iterations of long computation task
		/// </summary>
		public static void TestOpenCLMatrixBig(){
			
			double time_cpu = 0.0;
			double time_gpu = 0.0;
			
			int ITER = 10;
			
			for(int i=0;i<ITER;++i){
				float[,] wm1_dev = new float[500,500];
				float[,] wm2_dev = new float[1000,500];
				
				WIOMatrixTests._FillRnd(wm1_dev);
				WIOMatrixTests._FillRnd(wm2_dev);
				float[,] wm1_dev_cl = (float[,])wm1_dev.Clone();
				float[,] wm2_dev_cl = new float[500,1000];
				for(int k=0;k<1000;++k) for(int j=0;j<500;++j) wm2_dev_cl[j,k] = wm2_dev[k,j];
				
				
	
				WIOOpenCLMatrix wm1 = new WIOOpenCLMatrix(wm1_dev);
				WIOOpenCLMatrix wm2 = new WIOOpenCLMatrix(wm2_dev);
				wm2.Transpose();
				
				System.DateTime start = System.DateTime.Now;
				
				
				WIOMatrix result = wm1 *wm2;
				time_gpu+= (System.DateTime.Now - start).TotalMilliseconds;
				start = System.DateTime.Now;
				float[,] result_dev = WIOMatrixTests._MatrixMultNaive(wm1_dev_cl,wm2_dev_cl);
				time_cpu+= (System.DateTime.Now - start).TotalMilliseconds;
				
				((WIOOpenCLMatrix)result).Update();
				
				Random rand = new Random();
				
				for(int x=0;x<100;++x){
					int row = rand.Next () % 100;
					int column = rand.Next() % 100;
					NUnit.Framework.Assert.AreEqual(result_dev[row, column], result[row+1,column+1]);
				}
				
				GC.SuppressFinalize(wm1_dev);
				GC.SuppressFinalize(wm2_dev);
				GC.SuppressFinalize(wm1_dev_cl);
				GC.SuppressFinalize(wm2_dev_cl);
			
				wm1.Dispose();
				wm2.Dispose();
				result.Dispose();
			}
			
			ToolkitGlobals.Logger.Log ("GPU TIME : "+time_gpu.ToString());
			ToolkitGlobals.Logger.Log ("CPU TIME : "+time_cpu.ToString());
		
		}
		
		
		

	}
}

