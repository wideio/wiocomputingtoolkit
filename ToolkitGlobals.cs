using System;
using OpenCL;
using OpenCL.Net;


using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Diagnostics;

using NUnit.Framework;

using System.IO;


using WIOGlobals;

namespace WIOComputingToolkit{
	
	
	public enum WIOComputingToolkitBackend{
		/// <summary>
		/// Only available backend for now. Anyway it is not well-generic coded so it doesn't matter for now. 
		/// CPU based algebra on some library should be added for platforms not supporting OpenCl.
		/// </summary>
		OPENCL 
	}
	
	// it is a little bit mess here, it is a prototype //
	public static class ToolkitGlobals{
		// We need multithreading access here
		private static SLogger _Logger = new SLogger("WIOOpenCLToolkit.log");

		
		//wrl disabled for simplicity
		public static SLogger Logger{
			get{
				
				object x;
//				wrl.AcquireReaderLock(100);
				x = _Logger;
//				wrl.ReleaseReaderLock();
				return (SLogger)x;
			}
			set{
//				wrl.AcquireWriterLock(100);
				_Logger = value;
//				wrl.ReleaseWriterLock();
			}
		}
	
		private static OpenCLNode _defaultOpenCLNode = null;
		
		public static OpenCLNode DefaultOpenCLNode{
			get{
				if(_defaultOpenCLNode == null){
					_defaultOpenCLNode = new OpenCLNode();
				}
				return _defaultOpenCLNode;
			}
		}
		
	}
	/// <summary>
	/// Open CL node - what I mean by that is that is single device 
	/// That is able to carry out kernel operations and
	/// has got compiled kernels for its implementation
	/// </summary>
	public class OpenCLNode
	{
		public bool Initialized;
		
		//this variables HAVE to be intialized in Initialize() operation
		public Cl.Context _context;
		public Cl.Device _device;
		
		//TODO: change to private, add kernels compilation here
		public Cl.CommandQueue _cmdQueue;
	
		public Dictionary<string, Cl.Kernel> compiledKernels = new Dictionary<string, Cl.Kernel>();
		
		public string IncludeKernel = "";
		
		public void BuildKernels(){
		
		
			foreach(string f in Directory.GetFiles ("./Kernels")){
			
				if(!f.EndsWith("cl")) continue;
				
				string tmp = f.Split('/')[f.Split ('/').Length-1].Substring(0,(f.Split('/')[f.Split ('/').Length-1].Length-3));
				
				
				
				this.GetKernel(tmp);
			
			}
		
		}
		
		public void mycallback(Cl.Program program, IntPtr userdata){
			mre.Reset();
		}
		ManualResetEvent mre = new ManualResetEvent(false);
		public Cl.Kernel GetKernel(string name){
			if(this.compiledKernels.ContainsKey(name)) return compiledKernels[name];
			
			Cl.ErrorCode error;				
			
			Cl.Program program = Cl.CreateProgramWithSource(
					
				this._context, 1, new[] {this.IncludeKernel+"\n"+ File.ReadAllText("./Kernels/"+name+".cl") }, null, out error);
				
			try{
			
				error = Cl.BuildProgram(program, 1,
				                        new[] {ToolkitGlobals.DefaultOpenCLNode._device },
										string.Empty, 
				mycallback, IntPtr.Zero);
				
				
				
				
			
				
				mre.Set ();
				
				if(name=="matrix_convl"){
					mre.Reset ();
				}
		
				NUnit.Framework.Assert.IsTrue(error == Cl.ErrorCode.Success);
				
				Cl.Kernel[] kernels = Cl.CreateKernelsInProgram(program, out error);

				
				Cl.Kernel kernel = kernels[0];	 //we assume one kernel per program
				
				compiledKernels[name] = kernel;
			
			}
			catch(Exception e){
				string tmp = this.IncludeKernel+"/n"+ File.ReadAllText("./Kernels/"+name+".cl");
				Cl.InfoBuffer ib =  Cl.GetProgramBuildInfo
					(program,
				     this._device, 
				                                           
				           Cl.ProgramBuildInfo.Log                           
				       ,out error); 
				Console.WriteLine (tmp.ToString());
				Console.WriteLine (ib.ToString());
				ToolkitGlobals.Logger.LogError("Compilation error "+e.ToString());
				throw e;
			}
			return compiledKernels[name];
			
		}
		
		
	
		/// <summary>
		/// Initialize environment for GPU computing on this specific platform
		/// 
		/// This function makes few sanity checks, and after loads context etc.
		/// 
		/// </summary>
		public void Initialize(){
			if(this.Initialized) return;
			
			uint platformCount;
            Cl.ErrorCode result = Cl.GetPlatformIDs(0, null, out platformCount);
            Debug.Assert (result==Cl.ErrorCode.Success, "Could not get platform count");
            Console.WriteLine("{0} platforms found", platformCount);
			
			var platformIds = new Cl.Platform[platformCount];
            result = Cl.GetPlatformIDs(platformCount, platformIds, out platformCount);
            Debug.Assert (result==Cl.ErrorCode.Success, "Could not get platform ids");

			
			
			
			
            foreach (Cl.Platform platformId in platformIds)
            {
                IntPtr paramSize;
                result = Cl.GetPlatformInfo(platformId, Cl.PlatformInfo.Name, IntPtr.Zero, Cl.InfoBuffer.Empty, out paramSize);
                Debug.Assert (result==Cl.ErrorCode.Success, "Could not get platform name size");

                using (var buffer = new Cl.InfoBuffer(paramSize))
                {
                    result = Cl.GetPlatformInfo(platformIds[0], Cl.PlatformInfo.Name, paramSize, buffer, out paramSize);
                    Debug.Assert (result==Cl.ErrorCode.Success, "Could not get platform name string");
                }

                uint deviceCount;
                result = Cl.GetDeviceIDs(platformIds[0], Cl.DeviceType.All, 0, null, out deviceCount);
                 Debug.Assert (result==Cl.ErrorCode.Success, "Could not get device count");

				ToolkitGlobals.Logger.Log ("DeviceCount:"+deviceCount.ToString());
				
				
                var deviceIds = new Cl.Device[deviceCount];
                result = Cl.GetDeviceIDs(platformIds[0], Cl.DeviceType.All, deviceCount, deviceIds, out deviceCount);
                 Debug.Assert (result==Cl.ErrorCode.Success,"Could not get device ids");

                result = Cl.GetDeviceInfo(deviceIds[0], Cl.DeviceInfo.Vendor, IntPtr.Zero, Cl.InfoBuffer.Empty, out paramSize);
                 Debug.Assert (result==Cl.ErrorCode.Success, "Could not get device vendor name size");
                using (var buf = new Cl.InfoBuffer(paramSize))
                {
                    result = Cl.GetDeviceInfo(deviceIds[0], Cl.DeviceInfo.Vendor, paramSize, buf, out paramSize);
                     Debug.Assert (result==Cl.ErrorCode.Success, "Could not get device vendor name string");
                    var deviceVendor = buf.ToString();
					ToolkitGlobals.Logger.Log ("Vendor:"+deviceVendor.ToString());
                }
            }
			
			//Testing pased : seems to be fine, let's go straight to initialization:
			
			/* Main initialization procedure*/
			
			Cl.ErrorCode error;
			
			this._device = (from device in
                           Cl.GetDeviceIDs(
                               (from platform in Cl.GetPlatformIDs(out error)
                                where Cl.GetPlatformInfo(platform, Cl.PlatformInfo.Name, out error).ToString() == "NVIDIA CUDA"
                                select platform).First(), Cl.DeviceType.Gpu, out error)
                       select device).First();
			Int32 size = Cl.GetDeviceInfo(this._device,Cl.DeviceInfo.GlobalMemSize,out error).CastTo<Int32>();
			
			ToolkitGlobals.Logger.Log("Global mem cache size : "+size.ToString());
			
			Int32 sizewg = Cl.GetDeviceInfo(this._device,Cl.DeviceInfo.MaxWorkGroupSize,out error).CastTo<Int32>();
			
			ToolkitGlobals.Logger.Log("Maximum workgroup size : "+sizewg.ToString());			
			Debug.Assert(error == Cl.ErrorCode.Success);
            this._context = Cl.CreateContext(null, 1, new[] { _device }, null, IntPtr.Zero, out error);
			Debug.Assert(error == Cl.ErrorCode.Success);
			
			this._cmdQueue = Cl.CreateCommandQueue(ToolkitGlobals.DefaultOpenCLNode._context,  
			                                                 ToolkitGlobals.DefaultOpenCLNode._device, 
			                                                 (Cl.CommandQueueProperties)0, 
			                                                 out error);
			this.Initialized = true;
			
			ToolkitGlobals.Logger.Log("Complete OpenCL initialization complete");
			
			
			this.IncludeKernel = File.ReadAllText("./Kernels/gen.h");
//			this.BuildKernels();
			
			ToolkitGlobals.Logger.Log("Prebuilding Kernels finished");
			

			
		}
	}
}

