using System;
using WIOGlobals;

using OpenCL.Net;

using Emgu.CV;
using Emgu.CV.Util;

using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

using System.Threading;

using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Drawing.Drawing2D;

namespace WIOComputingToolkit
{
	public static class Utils
	{
	
		public unsafe static float[,] Float2DMatrixFromBitmap(System.Drawing.Bitmap source, int channel = 1){		
			
			float [,] result = new float[source.Height, source.Width];
			
			BitmapData bData = source.LockBits(new Rectangle(0, 0, source.Width, source.Height), ImageLockMode.ReadWrite, source.PixelFormat);
			Console.WriteLine(source.PixelFormat.ToString());
			 
			byte bitsPerPixel = 4*8;
			byte* scan0 = (byte*)bData.Scan0.ToPointer();
			
			for(int i=0;i<source.Width;++i)
				for(int j=0;j<source.Height;++j){
					byte* data = scan0 + (j) * bData.Stride + (i) * bitsPerPixel / 8;
					if(channel == 1) result[j,i] = data[1];
					else if(channel == 2) result[j,i] = data[2];
					else if(channel == 3) result[j,i] = data[3];
				}	
		
			
			return result;
		}

		public unsafe static WIOOpenCLMatrix WIOOpenCLMatrixFromBitmap(System.Drawing.Bitmap source, int channel = 1){		
			WIOOpenCLMatrix result = new WIOOpenCLMatrix(source.Height, source.Width);
			BitmapData bData = source.LockBits(new Rectangle(0, 0, source.Width, source.Height), ImageLockMode.ReadWrite, source.PixelFormat);
			Console.WriteLine(source.PixelFormat.ToString());
			
			byte bitsPerPixel = 4*8;
			byte* scan0 = (byte*)bData.Scan0.ToPointer();
			
			for(int i=0;i<source.Width;++i)
				for(int j=0;j<source.Height;++j){
					byte* data = scan0 + (j) * bData.Stride + (i) * bitsPerPixel / 8;
					if(channel == 1) result[j+1,i+1] = data[1];
					else if(channel == 2) result[j+1,i+1] = data[2];
					else if(channel == 3) result[j+1,i+1] = data[3];
				}

			return result;	
		}
		public static void DisplayOpenCLImage(Bitmap source, string winname = "", int waitkey = 30){	
			String win1 = winname=="" ? Utils.GetCurrentTimestamp().ToString() : winname;
			CvInvoke.cvNamedWindow (win1);	
			Image<Bgra, Byte> img = new Image<Bgra,Byte>((Bitmap)source);
		
			CvInvoke.cvShowImage (win1,img.Ptr);
			CvInvoke.cvWaitKey(waitkey);
		}
		public unsafe static void DisplayOWIOOpenCLMatrixAsImage(WIOOpenCLMatrix M){
				

			Bitmap bmp = new Bitmap(M.GetLength(1), M.GetLength(0));
			
					
			Console.WriteLine("Pixel format is {0}",bmp.PixelFormat.ToString());
			BitmapData bData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), 
			                                ImageLockMode.ReadWrite, bmp.PixelFormat);
			byte bitsPerPixel =4*8;//GetBitsPerPixel(bData.PixelFormat);
	
			Console.WriteLine(bmp.PixelFormat.ToString());
			
			byte* scan0 = (byte*)bData.Scan0.ToPointer();
			
			for(int i=1;i<=M.GetLength(1);++i) {
				for(int j=1;j<=M.GetLength(0);++j){
					byte* data = scan0 + (j-1) * bData.Stride + (i-1) * bitsPerPixel / 8;
					data[0] = (byte)M[j,i];
					data[1] = (byte)M[j,i];
					data[2] = (byte)M[j,i];
				}
			}
			bmp.UnlockBits(bData);
					
			Utils.DisplayOpenCLImage(bmp);

		}

		
		public unsafe static void DisplayOWIOOpenCLMatricesAsImage(WIOOpenCLMatrix R, WIOOpenCLMatrix G, WIOOpenCLMatrix B){
			throw new NotImplementedException();
		}		
		
		public static long GetCurrentTimestamp(){
		
			long ticks = DateTime.UtcNow.Ticks - DateTime.Parse("01/01/2000 00:00:00").Ticks;
			ticks /= 100; //Convert windows ticks to seconds
			return ticks;
		}
		
		
	}
}

