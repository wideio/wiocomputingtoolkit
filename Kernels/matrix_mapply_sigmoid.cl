__kernel void mapply_sigmoid(__global float * M, int tran)
{
    int i = get_global_id(0);
    int j = get_global_id(1);

    int rows= get_global_size(0);
    int cols = get_global_size(1);

    int index = get_index2full(i,j,rows,cols,tran);

	M[index] = 1.0f / (1.0f + exp(-(M[index])));
} 



