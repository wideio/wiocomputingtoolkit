__kernel void
floatMatrixSubstract( __global float *MRes,
                    const __global       float * M1,
                     const __global       float * M2, int tran1, int tran2)
{
    int i = get_global_id(0);
    int j = get_global_id(1);

    int p = get_global_size(0);
    int q = get_global_size(1);

    int index_tran0 = get_index2(i,j,p,0);
    int index1 = get_index2full(i,j,p,q,tran1);
    int index2 = get_index2full(i,j,p,q,tran2);

    MRes[index_tran0] = M1[index1] - M2[index2];
} 
