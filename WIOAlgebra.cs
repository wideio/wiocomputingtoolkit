using System;
using System.Linq;
using NUnit.Framework;
using System.Runtime.InteropServices;
using System.Diagnostics;
using WIOGlobals;
using OpenCL.Net;

using Emgu.CV;
using Emgu.CV.Util;

using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

using System.Drawing;
namespace WIOComputingToolkit
{
	
	public abstract class WIOMatrix : IDisposable{
		public abstract int GetLength(int dim);
		public abstract void Dispose();

		
		public abstract void RewriteData(float[,] devData);
		
		
		
		public abstract float ReduceSum();
		
		
		public abstract WIOMatrix Convolve(WIOMatrix mask);

		/// <summary>
		/// Apply function over all matrix elements
		/// </summary>
		public abstract void MApplySigmoid();
		
		/// <summary>
		/// Apply function over all matrix elements
		/// </summary>
		public  abstract void MApplyLinear(float a, float b = 0.0f);
		/// <summary>
		/// Apply function over all matrix elements
		/// </summary>
		public abstract void MApplyExp();

		public abstract void Transpose();
	
		//Why operators in C# are overloaded not overriden. It is plain stupid..
		
		
		public abstract WIOMatrix Substract(WIOMatrix b, bool _self = true, WIOMatrix dest = null);

		public abstract WIOMatrix Add(WIOMatrix b, bool _self = true, WIOMatrix dest = null);
		
		public abstract WIOMatrix Multiply(WIOMatrix b, bool _self = true, WIOMatrix dest = null);
	
		public static WIOMatrix operator +(WIOMatrix a, WIOMatrix b){
			return a.Add (b, _self : false);
		}
		public static WIOMatrix operator *(WIOMatrix a, WIOMatrix b){
			return a.Multiply(b, _self :false);
		}		
		public static WIOMatrix operator -(WIOMatrix a, WIOMatrix b){
			return a.Substract(b, _self : false);
		}
		
		public abstract float GetElement(int x, int y);
		public abstract void SetElement(int x, int y, float el);
		

		public float this[int x, int y]{
			get{
				return this.GetElement(x,y);
			}
			set{
				this.SetElement(x,y,value);
			}
		}

		
	}
	
	
	/// <summary>
	/// WIO matrix - the point of this class is to provide fast operations on GPU
	/// VERY SIMPLE, no C#/LINq magic - i don't think we need something fancy here
	/// TODO: for now no generics for simplicity
	/// TODO: think hard at multithreading : for now no multiaccess.. just as in float[,] maybe
	/// </summary>
	public class WIOOpenCLMatrix : WIOMatrix, OpenCLBufferable, IDisposable  //: ICloneable, IEnumerable, IList, IArray
	{			
		
		#region MATRIX_OP
		
		public override void Transpose(){
			this._transpose_state = (this._transpose_state+1)%2;
		}
		/// <summary>
		/// Performs reduce operation on matrix. TODO: pass C++ code, for now it is only in preprepared kernels
		/// </summary>
		public override float ReduceSum(){
			Assert.IsTrue (this.GetLength(1)==1); //Ensure we are doing reduce on vector
			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(this);
			
			
			int MAXGROUPSIZE = 1024; //TODO: getter in OpenCLNode
			int CUTOFFSIZE = 10000; 
			int current_size = this.GetLength(0);
		
			/* Prepare loop invariants */
			WIOOpenCLMatrix mid_result = null;
			WIOOpenCLMatrix current_matrix = this;

			
			/* Prepare OpenCL stuff */						
			Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("vector_reduce_sum");
			Cl.Event clevent;
			Cl.ErrorCode error;
			
			while(current_size > CUTOFFSIZE){	
				
				mid_result = new WIOOpenCLMatrix(1 + (current_matrix.GetLength(0) / MAXGROUPSIZE),1); 
				
				
				
				/* Just shouldn't be here*/
				int intPtrSize = 0; //TODO: move this code somewhere
	            intPtrSize = Marshal.SizeOf(typeof(IntPtr));
				
				/*Setting arguments*/
				Cl.SetKernelArg(kernel,0, new IntPtr(intPtrSize), current_matrix._buffer.BufferPtr);
				Cl.SetKernelArg(kernel,1, new IntPtr(sizeof(float) * MAXGROUPSIZE), null);
				Cl.SetKernelArg(kernel,2, current_matrix.GetLength(0));
				Cl.SetKernelArg(kernel,3, new IntPtr(intPtrSize), mid_result._buffer.BufferPtr);
			
	            error = Cl.EnqueueNDRangeKernel(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 1, null, new IntPtr[] 
				               { new IntPtr(MAXGROUPSIZE * mid_result.GetLength(0))},
							     new IntPtr[]{new IntPtr(MAXGROUPSIZE)}, 
							0, null, out clevent);
				
	            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
				
				//How to make it work?
//				if(current_matrix != this) current_matrix.Dispose(); //we might want to move it from this loop if there is enough memory
				
				/* Prepare loop invariants */
				current_matrix = mid_result;
				current_size = mid_result.GetLength(0);
            	
			
				current_matrix._buffer.SetChangeSwitch(true);
				
			}
		
			GPUResourceManager.EnsureCPUConsistencyOpenCLBufferable(current_matrix);
			Cl.Finish (ToolkitGlobals.DefaultOpenCLNode._cmdQueue);
			
			
			
			/* CPU Reduce */
			float result = 0.0f;
			for(int i=1; i <= current_matrix.GetLength(0); ++i) result += current_matrix[i, 1];
			
			//TODO: dispose current_matrix
					                                 
			return result;			
		}
		
		
		public override WIOMatrix Convolve(WIOMatrix mask){
			Assert.IsTrue (mask is WIOOpenCLMatrix);
			Assert.IsTrue (mask.GetLength(0)<=this.GetLength(0) && mask.GetLength(1) <= this.GetLength(1));
			Assert.IsTrue (mask.GetLength(0) == mask.GetLength(1));
			
			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(this);
			
			/*Calculate and create new matrix*/
			int r_out = this.GetLength(0) - (mask.GetLength(0)-1), c_out = this.GetLength (1) - (mask.GetLength (1) -1 );
			WIOOpenCLMatrix result = new WIOOpenCLMatrix(r_out, c_out);
			
			
			
			/*Call OpenCL kernel to calculate convolution*/
			Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("matrix_convl");
			Cl.Event clevent;
			Cl.ErrorCode error;
		
			int intPtrSize = 0;
            intPtrSize = Marshal.SizeOf(typeof(IntPtr));
			
			/*Setting arguments*/
			Cl.SetKernelArg(kernel,0, new IntPtr(intPtrSize), this._buffer.BufferPtr);
			Cl.SetKernelArg(kernel,1, new IntPtr(intPtrSize), ((WIOOpenCLMatrix)mask)._buffer.BufferPtr);
			Cl.SetKernelArg(kernel,2, new IntPtr(intPtrSize), result._buffer.BufferPtr);
			Cl.SetKernelArg(kernel,3,  0);
			Cl.SetKernelArg(kernel,4, 0);
			Cl.SetKernelArg(kernel,5,  this.GetLength(0));
			Cl.SetKernelArg(kernel,6,  this.GetLength(1));
			Cl.SetKernelArg(kernel,7,  this._transpose_state);
			Cl.SetKernelArg(kernel,8, mask.GetLength(0));
			

			
            error = Cl.EnqueueNDRangeKernel(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 2, null, new IntPtr[] 
			                                { new IntPtr(r_out), new IntPtr(c_out) }, null, 0, null, out clevent);
            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
			
			result._buffer.SetChangeSwitch(true); //it has changed
	
			
			return result;
		}


		
		/// <summary>
		/// Apply function over all matrix elements
		/// </summary>
		public override void MApplySigmoid(){
			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(this);
			
			
		  	Cl.ErrorCode error;				
			Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("matrix_mapply_sigmoid");
			int intPtrSize = 0;
            intPtrSize = Marshal.SizeOf(typeof(IntPtr));			
			Cl.SetKernelArg(kernel,0, new IntPtr(intPtrSize), this._buffer.BufferPtr);
			Cl.SetKernelArg(kernel,1,  this._transpose_state);
		
			Cl.Event clevent;
		

            error = Cl.EnqueueNDRangeKernel(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 2, null, new IntPtr[] 
			                                { new IntPtr(this.GetLength(0)), new IntPtr(this.GetLength(1)) }, null, 0, null, out clevent);

            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
			
			this._buffer.SetChangeSwitch(true);
		}
		
		/// <summary>
		/// Apply function over all matrix elements
		/// </summary>
		public override void MApplyLinear(float a, float b = 0.0f){
			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(this);
		
		  	Cl.ErrorCode error;				
			Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("matrix_mapply_linear");
			int intPtrSize = 0;
            intPtrSize = Marshal.SizeOf(typeof(IntPtr));			
			Cl.SetKernelArg(kernel,0, new IntPtr(intPtrSize), this._buffer.BufferPtr);
			Cl.SetKernelArg(kernel,1, new IntPtr(sizeof(float)), a);
			Cl.SetKernelArg(kernel,2, new IntPtr(sizeof(float)), b);
					
				Cl.SetKernelArg(kernel,3,  this._transpose_state);
			Cl.Event clevent;

        	
		
            error = Cl.EnqueueNDRangeKernel(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 2, null, new IntPtr[] 
			                                { new IntPtr(this.GetLength(0)), new IntPtr(this.GetLength(1)) }, null, 0, null, out clevent);

            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
			
			this._buffer.SetChangeSwitch(true);
		}		
		
		/// <summary>
		/// Apply function over all matrix elements
		/// </summary>
		public override void MApplyExp(){
			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(this);
		
		  	Cl.ErrorCode error;				
			Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("matrix_mapply_exp");
			int intPtrSize = 0;
            intPtrSize = Marshal.SizeOf(typeof(IntPtr));			
			Cl.SetKernelArg(kernel,0, new IntPtr(intPtrSize), this._buffer.BufferPtr);
			Cl.SetKernelArg(kernel,1,  this._transpose_state);
		
			Cl.Event clevent;

        	
		
            error = Cl.EnqueueNDRangeKernel(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 2, null, new IntPtr[] 
			                                { new IntPtr(this.GetLength(0)), new IntPtr(this.GetLength(1)) }, null, 0, null, out clevent);

            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
			
			this._buffer.SetChangeSwitch(true);
		}		

		
		/// <summary>
		/// Adds to itself matrix. Why there is no -= overloading in C#..
		/// </summary>
		/// <param name='b'>
		/// Matrix to be added
		/// </param>
		public override WIOMatrix Substract(WIOMatrix input, bool _self = true, WIOMatrix dest = null){
			WIOOpenCLMatrix b = (WIOOpenCLMatrix)input;
			
			if(_self == true){
			
			
				GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(this);
				GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(b);

			  	Cl.ErrorCode error;				
				Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("matrix_substract_self");
	
	
			
				int intPtrSize = 0;
	            intPtrSize = Marshal.SizeOf(typeof(IntPtr));
				
				Cl.SetKernelArg(kernel,0, new IntPtr(intPtrSize), this._buffer.BufferPtr);
				Cl.SetKernelArg(kernel,1, new IntPtr(intPtrSize), b._buffer.BufferPtr);
				Cl.SetKernelArg(kernel,2,  this._transpose_state);
				Cl.SetKernelArg(kernel,3,  b._transpose_state);
	
				
				Cl.Event clevent;
				
	          
			   // execute kernel
	            error = Cl.EnqueueNDRangeKernel(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 2, null, new IntPtr[] 
				                                { new IntPtr(this.GetLength(0)), new IntPtr(b.GetLength(1)) }, null, 0, null, out clevent);
	
	            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
	
				
				this._buffer.SetChangeSwitch(true); //it has changed
				return this;	
			}else{
				
				WIOOpenCLMatrix a = this;
				
					//note: every operation on GPU with bufferable objects should follow this convention.
					//first ensure consistency (invariant on start)
					//at the end ensure consistency of the result (invariant on the end)
					//with the assumption that no thread will ever modify the mid-results!
					
					Assert.IsTrue(a.GetLength(1) == b.GetLength(1) && a.GetLength(0) == b.GetLength(0));
					GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(a);
					GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(b);
					
				WIOOpenCLMatrix result = dest==null?new WIOOpenCLMatrix(a.GetLength(0), b.GetLength(1)) : (WIOOpenCLMatrix)dest;
				result.ChangeSwitch = false;
				result._buffer.ChangeSwitch = false;
				  	Cl.ErrorCode error;				
					Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("matrix_substract");
		
				
		
				
					int intPtrSize = 0;
		            intPtrSize = Marshal.SizeOf(typeof(IntPtr));
					
					Cl.SetKernelArg(kernel,0, new IntPtr(intPtrSize), result._buffer.BufferPtr);
					Cl.SetKernelArg(kernel,1, new IntPtr(intPtrSize), a._buffer.BufferPtr);
					Cl.SetKernelArg(kernel,2, new IntPtr(intPtrSize), b._buffer.BufferPtr);
					Cl.SetKernelArg(kernel,3,  this._transpose_state);
					Cl.SetKernelArg(kernel,4,  b._transpose_state);
	
					
					Cl.Event clevent;
					
		           
				   // execute kernel
		            error = Cl.EnqueueNDRangeKernel(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 2, null, new IntPtr[] 
					                                { new IntPtr(a.GetLength(0)), new IntPtr(b.GetLength(1)) }, null, 0, null, out clevent);
		
		            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
		
					
					result._buffer.SetChangeSwitch(true); //it has changed
					return result;
			}
		}
				

		/// <summary>
		/// Adds to itself matrix. Why there is no += overloading in C#..
		/// </summary>
		/// <param name='b'>
		/// Matrix to be added
		/// </param>
		public override WIOMatrix Add(WIOMatrix input, bool _self = true, WIOMatrix dest = null){
			WIOOpenCLMatrix b = (WIOOpenCLMatrix)input;
			if(_self){
				GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(this);
				GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(b);

			  	Cl.ErrorCode error;				
				Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("matrix_add_self");
	

	       
			
				int intPtrSize = 0;
	            intPtrSize = Marshal.SizeOf(typeof(IntPtr));
				
				Cl.SetKernelArg(kernel,0, new IntPtr(intPtrSize), this._buffer.BufferPtr);
				Cl.SetKernelArg(kernel,1, new IntPtr(intPtrSize), b._buffer.BufferPtr);
				Cl.SetKernelArg(kernel,2,  this._transpose_state);
				Cl.SetKernelArg(kernel,3,  b._transpose_state);
		
				
				Cl.Event clevent;
						
			   // execute kernel
	            error = Cl.EnqueueNDRangeKernel(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 2, null, new IntPtr[] 
				                                { new IntPtr(this.GetLength(0)), new IntPtr(b.GetLength(1)) }, null, 0, null, out clevent);
	
	            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
	
				
				this._buffer.SetChangeSwitch(true); //it has changed
				return this;
			}else{
				//note: every operation on GPU with bufferable objects should follow this convention.
				//first ensure consistency (invariant on start)
				//at the end ensure consistency of the result (invariant on the end)
				//with the assumption that no thread will ever modify the mid-results!
				WIOOpenCLMatrix a = this;
				Assert.IsTrue(a.GetLength(1) == b.GetLength(1) && a.GetLength(0) == b.GetLength(0));
				
			
					
				GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(a);
				GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(b);
				
			
				WIOOpenCLMatrix result = dest==null?new WIOOpenCLMatrix(a.GetLength(0), b.GetLength(1)) : (WIOOpenCLMatrix)dest;
				result.ChangeSwitch = false;
				result._buffer.ChangeSwitch = false;
				
			  	Cl.ErrorCode error;				
				Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("matrix_add");
	
			
		
			
				int intPtrSize = 0;
	            intPtrSize = Marshal.SizeOf(typeof(IntPtr));
				
				Cl.SetKernelArg(kernel,0, new IntPtr(intPtrSize), result._buffer.BufferPtr);
				Cl.SetKernelArg(kernel,1, new IntPtr(intPtrSize), a._buffer.BufferPtr);
				Cl.SetKernelArg(kernel,2, new IntPtr(intPtrSize), b._buffer.BufferPtr);
				Cl.SetKernelArg(kernel,3,  a._transpose_state);
				Cl.SetKernelArg(kernel,4,  b._transpose_state);
	
				
				
				Cl.Event clevent;
					
			   // execute kernel
	            error = Cl.EnqueueNDRangeKernel(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 2, null, new IntPtr[] 
				                                { new IntPtr(a.GetLength(0)), new IntPtr(b.GetLength(1)) }, null, 0, null, out clevent);
	
	            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
	
				
				result._buffer.SetChangeSwitch(true); //it has changed
				return result;			
			}
		}
		
		public override WIOMatrix Multiply (WIOMatrix input, bool _self = true, WIOMatrix dest = null)
		{
			WIOOpenCLMatrix b = (WIOOpenCLMatrix)input;
			if(!_self){
							//note: every operation on GPU with bufferable objects should follow this convention.
				//first ensure consistency (invariant on start)
				//at the end ensure consistency of the result (invariant on the end)
				//with the assumption that no thread will ever modify the mid-results!
				WIOOpenCLMatrix a = this;
				
				
	
				Assert.IsTrue(a.GetLength(1) == b.GetLength(0));
				
				
				GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(a);
				GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(b);
				
				WIOOpenCLMatrix result = dest==null?new WIOOpenCLMatrix(a.GetLength(0), b.GetLength(1)) : (WIOOpenCLMatrix)dest;
				result.ChangeSwitch = false;
				result._buffer.ChangeSwitch = false;
				
			  	Cl.ErrorCode error;				
				Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("matrix_mult");
	
			
	
	       
			
				int intPtrSize = 0;
	            intPtrSize = Marshal.SizeOf(typeof(IntPtr));
				
				Cl.SetKernelArg(kernel,0, new IntPtr(intPtrSize), result._buffer.BufferPtr);
				Cl.SetKernelArg(kernel,1, new IntPtr(intPtrSize), a._buffer.BufferPtr);
				Cl.SetKernelArg(kernel,2, new IntPtr(intPtrSize), b._buffer.BufferPtr);
				Cl.SetKernelArg(kernel,3,  a._transpose_state);
				Cl.SetKernelArg(kernel,4,  b._transpose_state);
				Cl.SetKernelArg(kernel,5, a.GetLength(1));
				Cl.SetKernelArg(kernel,6, a._rows_dev);
				Cl.SetKernelArg(kernel,7, b._rows_dev);
				
				
				Cl.Event clevent;
				
	          
			   // execute kernel
	            error = Cl.EnqueueNDRangeKernel(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 2, null, new IntPtr[] 
				                                { new IntPtr(a.GetLength(0)), new IntPtr(b.GetLength(1)) }, null, 0, null, out clevent);
	
	            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
	
				
				result._buffer.SetChangeSwitch(true); //it has changed
				return result;
			}else{
				throw new NotImplementedException();
			}
		}

		
		
		public override float GetElement (int x, int y)
		{
			GPUResourceManager.EnsureCPUConsistencyOpenCLBufferable(this);
			if(this._transpose_state==0) return this._devData[x-1,y-1];
			else return this._devData[y-1,x-1];
		}
		public override void SetElement (int x, int y, float value)
		{
			GPUResourceManager.EnsureCPUConsistencyOpenCLBufferable(this);
			
			if(this._transpose_state==0)  this._devData[x-1,y-1]= value;
			else this._devData[y-1,x-1]= value;
	
			this.ChangeSwitch = true;
		}
	
		private float _GetElementCPU (int x, int y)
		{
			if(this._transpose_state==0) return this._devData[x-1,y-1];
			else return this._devData[y-1,x-1];
		}
		private void _SetElementCPU (int x, int y, float value)
		{
			if(this._transpose_state==0)  this._devData[x-1,y-1]= value;
			else this._devData[y-1,x-1]= value;;
	
			this.ChangeSwitch = true;
		}		
		/// <summary>
		/// Get element directly from GPU memory without retrieving whole object to memory
		/// </summary>
		/// <returns>
		public float GetElementGPU (int row, int col)
		{
			GPUResourceManager.EnsureGPUConsistencyOpenCLBufferable(this);
	
		  	Cl.ErrorCode error;				
			Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("matrix_getel");
			int intPtrSize = 0;
            intPtrSize = Marshal.SizeOf(typeof(IntPtr));			
			Cl.SetKernelArg(kernel,0, new IntPtr(intPtrSize), this._buffer.BufferPtr);
			Cl.SetKernelArg(kernel,1, row-1);
			Cl.SetKernelArg(kernel,2, col-1);
			Cl.SetKernelArg(kernel,3, this._transpose_state);
			Cl.SetKernelArg(kernel,4, this._rows_dev);
			Cl.SetKernelArg(kernel, 5 , new IntPtr(intPtrSize), GPUResourceManager.READ_FLOAT_BUF);
		
			Cl.Event clevent;

		
            error = Cl.EnqueueNDRangeKernel(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 1, null, new IntPtr[] 
			                                { new IntPtr(1) }, null, 0, null, out clevent);

            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
			
			this._buffer.SetChangeSwitch(true);		
			
			return GPUResourceManager.GetFLOATBUFValue();
		}
		/// <summary>
		/// Set element directly on GPU memory without retrieving whole object to memory
		/// </summary>
		/// <returns>
		public void SetElementGPU (int row, int col, float value)
		{
			GPUResourceManager.EnsureCPUConsistencyOpenCLBufferable(this);
		  	
			
			Cl.ErrorCode error;				
			Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("matrix_setel");
			int intPtrSize = 0;
            intPtrSize = Marshal.SizeOf(typeof(IntPtr));			
			Cl.SetKernelArg(kernel,0, new IntPtr(intPtrSize), this._buffer.BufferPtr);
			Cl.SetKernelArg(kernel,1, row-1);
			Cl.SetKernelArg(kernel,2, col-1);
			Cl.SetKernelArg(kernel,3, this._transpose_state);
			Cl.SetKernelArg(kernel,4, this._rows_dev);
			Cl.SetKernelArg(kernel,5, value);
			Cl.Event clevent;

        	
		
            error = Cl.EnqueueNDRangeKernel(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 1, null, new IntPtr[] 
			                                { new IntPtr(1) }, null, 0, null, out clevent);

            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
			
			this._buffer.SetChangeSwitch(true);	
		}	
		
		#endregion MATRIX_OP
		
		#region CONSTRUCTORS
		
		/// <summary>
		/// Rewrites the data.
		/// </summary>
		public override void RewriteData(float[,] devData){
			if(this._transpose_state == 1 ) throw new NotImplementedException();
			
			
			this._devData = (float[,]) devData.Clone();
			this._buffer.ChangeSwitch = false;
			this._rows_dev = this._devData.GetLength(0); //only here gets rewritten
			this.ChangeSwitch = true;
			GPUResourceManager.EnsureConsistencyOpenCLBufferable(this);
		}
		
		public WIOOpenCLMatrix(float[,] devData){
			this._devData = (float[,]) devData.Clone();
			this._rows_dev = this._devData.GetLength(0);
			this._buffer = new OpenCLResourceBufferHandle<float>(
				devData.GetLength(0)*devData.GetLength (1)
				);
			this._buffer.SetDual(this); //wire it
			this._buffer.ChangeSwitch = false;
			this.ChangeSwitch = true;
			
			GPUResourceManager.EnsureConsistencyOpenCLBufferable(this);
		}
		
		public WIOOpenCLMatrix (int N, int M)
		{
			this._devData = new float[N,M];
			this._rows_dev = N;
			for(int i=0;i<N;++i) for(int j=0;j<M;++j) this._devData[i,j]=0.0f;
			this._buffer = new OpenCLResourceBufferHandle<float>(N*M);
			this._buffer.SetDual(this); //wire it
			
			if(this._buffer.GetChangeSwitch()) this._buffer.SetChangeSwitch(false);
			this.ChangeSwitch = true;
			
			GPUResourceManager.EnsureConsistencyOpenCLBufferable(this);
		}
		
		public WIOOpenCLMatrix(OpenCLResourceBufferHandle<float> buffer, int N, int M){
			
			//that's a shame that C# doesnt have reshape in linq
			this._devData = new float[N,M];		
			this._rows_dev = this._devData.GetLength(0);
			this.ChangeSwitch = false;
			
			this._buffer = buffer;
			this._loadBuffer();
			this._buffer.ChangeSwitch = false;
			
			GPUResourceManager.EnsureConsistencyOpenCLBufferable(this);
		}
		
		#endregion CONSTRUCTORS
		
		#region INNER

		
		public object GetData(){
			return (object)this._dumpDevData();
		}
		
		public void Update(){
			GPUResourceManager.EnsureConsistencyWeakOpenCLBufferable(this);  //isn't failed?
			
			bool tmp = this._buffer.GetChangeSwitch();

			if(tmp==false) 
				return; 
			
			this._loadBuffer();
			this.ChangeSwitch = false;
			if(this._buffer.GetChangeSwitch()) this._buffer.SetChangeSwitch(false);
			
			GPUResourceManager.EnsureConsistencyOpenCLBufferable(this); //invariant
		}
		


		public OpenCLBufferable GetDual(){
			return (OpenCLBufferable)this._buffer;
		}
		//it is slave, buffer defines object in a way (maybe buffer is not the best name? storage?)
		public void SetDual(OpenCLBufferable x){
			this._buffer = (OpenCLResourceBufferHandle<float>)x;
			//reset state
			this._loadBuffer();
			this.ChangeSwitch = false;
			if(_buffer.GetChangeSwitch()) _buffer.SetChangeSwitch(false); 
	
			if((object)_buffer.GetDual()!=(object)this) this._buffer.SetDual((OpenCLBufferable)this); //just in case. shouldn't be here
		}
		
		public bool GetChangeSwitch(){
			return ChangeSwitch;
		}
		
		public void SetChangeSwitch(bool new_value){
			ChangeSwitch = new_value;
		}
		
		private int _getDevIndex(int row, int col){
			if(this._transpose_state == 0) return col*this._rows_dev + row;
			else return row*this._rows_dev + col;
		}
		
		//TODO: improve speed
		private float[] _dumpDevData(){ //TODO: memcpy here?
			
			int N = this.GetLength(0);
			int M = this.GetLength(1);
			float[] tmp = new float[N*M];
			for(int i=0;i<N;++i)
				for(int j=0;j<M;++j)
					//anyway for simplicity we are forgetting all threading, here should be WRL 
					tmp[_getDevIndex(i,j)] = this._GetElementCPU(i+1,j+1);
			return tmp;
		}		
		//TODO: improve speed
		private void _loadBuffer(){
			int N = this.GetLength(0);
			int M = this.GetLength(1);
			float[] tmp = this._buffer.ReadBuffer(N*M);
			for(int i=0;i<N;++i)
				for(int j=0;j<M;++j)
					//anyway for simplicity we are forgetting all threading, here should be WRL 
					this._SetElementCPU(i+1,j+1, tmp[_getDevIndex(i,j)]);		
		}
		
		
		public override void Dispose(){
			Dispose (true);
			GC.SuppressFinalize(this);
		}
		
		public void Dispose(bool disposing){
			if(!this._disposed){
				this._disposed = true;
				this._buffer.Dispose();
			}
		}
			 
		public override int GetLength(int dim){
			Assert.IsTrue(dim<= 1);
			return this.DevData.GetLength((dim+(this._transpose_state==1?1:0))%2);
		}
		
		// TODO: thing about how many threads could access this,?
		public float[,] DevData{
			get{ 
				return _devData;
			}
		}
			
					
		
		#endregion INNER
		
		#region FIELDS
		private OpenCLResourceBufferHandle<float> _buffer= null; // might want to buffer it somewhere - this is the reason for this field
		bool _disposed = false;
		int _transpose_state = 0;
		int _rows_dev = -1000000;
		public bool ChangeSwitch; //toggled if change occurs
		private float[,] _devData;
		#endregion FIELDS
	}
}

