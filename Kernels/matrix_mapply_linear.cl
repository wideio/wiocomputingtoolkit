__kernel void mapply_multiply(__global float * M, float a, float b, int tran)
{
    int i = get_global_id(0);
    int j = get_global_id(1);



    int rows = get_global_size(0);
    int cols = get_global_size(1);
    

    int index = get_index2full(i,j,rows,cols,tran);

	M[index] = M[index]*a + b;
} 
