using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using OpenCL.Net;
using System.IO;
using WIOGlobals;

namespace WIOComputingToolkit
{
	public class GeneralTests
	{
		
		public static void Setup(){
			ToolkitGlobals.DefaultOpenCLNode.Initialize();
			
		}
		
		public static void TestOpenCLBufferResource(){
			OpenCLResourceBufferHandle<float> CreatedBuffer = 
				new OpenCLResourceBufferHandle<float>(1000);
			
			float[] write = new float[]{2.0f,3.0f,4.0f};
			
			CreatedBuffer.WriteToBuffer(write);
			
			ToolkitGlobals.Logger.Log ("Read value from GPU = "+CreatedBuffer.ReadBuffer(3)[0].ToString());
		}
		
		public static void TestOpenCLBufferDuality(){
			int N = 10;
			
			//Create buffer
			OpenCLResourceBufferHandle<float> CreatedBuffer =
				new OpenCLResourceBufferHandle<float>(N);
			
			//Fill it up
			float[] write = new float[N];
			CreatedBuffer.WriteToBuffer(write);
			
			
			//Create matrix that will operate on this buffer, it is a slave, so it will load everything
			WIOOpenCLMatrix matrix = new WIOOpenCLMatrix(5,2);
			matrix.SetDual(CreatedBuffer);  

			
			//Buffer should reflect the change!
			matrix[1,2]=10;
			ToolkitGlobals.Logger.LogArray ((float[])matrix.GetData());
			CreatedBuffer.Update(); //for now it should be this way , for simplicity
			float[] T=CreatedBuffer.ReadBuffer(10);
			Assert.IsTrue(T[5]==10);
		}
		
		

		public static void TestInstallation(){
			ToolkitGlobals.DefaultOpenCLNode.Initialize();
		}
		
		/// <summary>
		/// Tests simple square function. If passes OpenCL is setup correctly for 99%
		/// Builds program, loads the memory and executes kernel
		/// </summary>
		public static void TestSimpleSquare(){
	 
            const int cnBlockSize = 900;
			const int cnBlocks = 900;
			
			const float x = 10.0f;
			
			
            IntPtr cnDimension = new IntPtr(cnBlocks * cnBlockSize);	
			
			
			
			
			ToolkitGlobals.Logger.Log("Allocating memory");
			float[] A =null;
			float[] B = null;
			try{
				 A = new float[cnDimension.ToInt32()];
            	 B = new float[cnDimension.ToInt32()];
			}catch(Exception e){
				Console.WriteLine(e.ToString());
			}
			
		  	Cl.ErrorCode error;				
			Cl.Kernel kernel = ToolkitGlobals.DefaultOpenCLNode.GetKernel("tester");

            // initialize host memory
            Random rand = new Random();
            for (int i = 0; i < A.Length; i++)
            {
                A[i] = rand.Next() % 256;
                B[i] = rand.Next() % 256;
            }			
			ToolkitGlobals.Logger.Log("Allocated memory");
		
			Cl.IMem<float> hDeviceMemA = Cl.CreateBuffer(ToolkitGlobals.DefaultOpenCLNode._context,
			                                             Cl.MemFlags.CopyHostPtr | Cl.MemFlags.ReadOnly, 
			                                             A, out error);
            Cl.IMem hDeviceMemB = Cl.CreateBuffer(ToolkitGlobals.DefaultOpenCLNode._context,
			                                      Cl.MemFlags.CopyHostPtr | Cl.MemFlags.WriteOnly, 
			                                      (IntPtr)(sizeof(float) * cnDimension.ToInt32()),                          
			                                      B, out error);
		
			int intPtrSize = 0;
            intPtrSize = Marshal.SizeOf(typeof(IntPtr));
			
			Cl.SetKernelArg(kernel,0, new IntPtr(intPtrSize), hDeviceMemA);
			Cl.SetKernelArg(kernel,1, new IntPtr(intPtrSize), hDeviceMemB);
			Cl.SetKernelArg(kernel,2, 
			                
			                new IntPtr(Marshal.SizeOf(typeof(int))),
			                 cnDimension.ToInt32());
			Cl.Event clevent;
			
            error = Cl.EnqueueWriteBuffer(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, hDeviceMemA, 
			                              Cl.Bool.True, IntPtr.Zero,
                    new IntPtr(cnDimension.ToInt32() * sizeof(float)),
                    A, 0, null, out clevent);
			
		
			ToolkitGlobals.Logger.Log("Kernel compiled and data writting is successful");	
			
           Assert.AreEqual(Cl.ErrorCode.Success, error);			
		   // execute kernel
            error = Cl.EnqueueNDRangeKernel(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, kernel, 1, null, new IntPtr[] 
			                                { cnDimension }, null, 0, null, out clevent);
			
			
			
            Assert.AreEqual(Cl.ErrorCode.Success, error, error.ToString());
			ToolkitGlobals.Logger.Log("Computation was successful");	
			
			error = Cl.EnqueueReadBuffer(ToolkitGlobals.DefaultOpenCLNode._cmdQueue, hDeviceMemB, Cl.Bool.True, 0, B.Length, B, 0, null, out clevent);

			foreach(var z in B){
				Assert.AreEqual(z,1);
			}
			
			Cl.Finish(ToolkitGlobals.DefaultOpenCLNode._cmdQueue);
			
            Cl.ReleaseMemObject(hDeviceMemA);
            Cl.ReleaseMemObject(hDeviceMemB);
			
		}
	}
}

